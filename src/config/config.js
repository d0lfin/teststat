/**
 * Created by dolf
 */
import { join } from 'path';

const MONGO_DB = 'teststat',
    MONGO_URL = 'mongodb://localhost/',
    SERVER_PORT = 8080,
    TESTPALM_API_ENTRY_POINT = 'https://testpalm.yandex-team.ru/api/',
    STAFF_API_ENTRY_POINT = 'https://staff-api.yandex-team.ru/v3/',
    TEAMCITY_API_ENTRY_POINT = 'http://teamcity.desktop.dev.yandex.net:8111/httpAuth/app/rest/',
    TEAMCITY_URL = 'http://teamcity.desktop.dev.yandex.net/',
    TESTPALM_URL = 'https://testpalm.yandex-team.ru/',
    TEAMCITY_USER = {
        login: 'browsergradle_custombuild',
        password: 'qweasdzxc'
    },
    SERVER_IP_ADDRESS = '127.0.0.1',
    DB = MONGO_URL + MONGO_DB,
    SERVER = {
        PORT: SERVER_PORT,
        IP: SERVER_IP_ADDRESS
    },
    OAUTH_AUTHORIZATION_URL = 'https://oauth.yandex-team.ru/authorize',
    OAUTH_TOKEN_URL = 'https://oauth.yandex-team.ru/token',
    OAUTH_CLIENT_ID = 'bc875ada698f46fe96487ac1ef50b227',
    OAUTH_CLIENT_SECRET = '66657018838f417684e7666c1d1a6500',
    OAUTH_CALLBACK_URL = '/oauth';

export {
    DB, SERVER, TESTPALM_API_ENTRY_POINT, TEAMCITY_API_ENTRY_POINT, TEAMCITY_URL, STAFF_API_ENTRY_POINT,
    TESTPALM_URL, TEAMCITY_USER, OAUTH_AUTHORIZATION_URL, OAUTH_TOKEN_URL, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET,
    OAUTH_CALLBACK_URL
};
