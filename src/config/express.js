/**
 * Created by dolf
 */
import { join } from 'path';
import express from 'express';
import bodyParser from 'body-parser';

function staticDir (dir) {
    return express.static(join(__dirname, '../..', dir));
}

export default function (app) {
    app.use(bodyParser.json());
    app.use('/js', staticDir('public/js'));
    app.use('/css', staticDir('public/css'));
    app.use('/html', staticDir('public/html'));
    app.use('/bower_components', staticDir('bower_components'));
};