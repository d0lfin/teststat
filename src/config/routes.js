/**
 * Created by dolf
 */
import wrap from 'express-async-wrap';
import { join } from 'path';
import functionality from '../app/controllers/functionality';
import project from '../app/controllers/project';
import settings from '../app/controllers/settings';
import autoruns from '../app/controllers/autoruns';
import releases from '../app/controllers/releases';
import versions from '../app/controllers/versions';
import topsuites from '../app/controllers/topsuites';
import nightlystat from '../app/controllers/nightlystat';
import Staff from '../app/services/staff';
import Testpalm from '../app/services/testpalm';
import Startrack from '../app/services/startrack';
import passport from 'passport';
import Settings from '../app/models/settings';
import { OAuth2Strategy } from 'passport-oauth';
import { OAUTH_AUTHORIZATION_URL, OAUTH_TOKEN_URL, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, OAUTH_CALLBACK_URL } from './config';

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use('provider', new OAuth2Strategy({
        authorizationURL: OAUTH_AUTHORIZATION_URL,
        tokenURL: OAUTH_TOKEN_URL,
        clientID: OAUTH_CLIENT_ID,
        clientSecret: OAUTH_CLIENT_SECRET,
        callbackURL: OAUTH_CALLBACK_URL
    },   
    function(accessToken, refreshToken, profile, done) {
        done(null, {
            token: accessToken
        });
    }
));

module.exports = function (app) {
    app.use(passport.initialize());
    app.use(passport.session());

    app.get('/', function (req, res){
        res.sendFile(join(__dirname, '../..', 'public/html/index.html'));
    });

    app.get('/login', passport.authenticate('provider'));
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    app.get('/oauth', passport.authenticate('provider', {
            successRedirect: '/#/projects',
            failureRedirect: '/login'
    }));

    app.all('/api/*', isAuthenticated);
    app.all('/api/*', wrap(async function (req, res, next) {
        let token = req.session.passport.user.token,
            settings = await Settings.load();
        settings.oauthToken = token;
        await settings.save();
        req.testpalm = new Testpalm(token);
        req.startrack = new Startrack(token);
        req.staff = new Staff(token);
        next();
    }));

    app.get('/api/projects', wrap(project.list));

    app.get('/api/project/:projectId', wrap(project.load));
    app.get('/api/project/:projectId', wrap(project.get));

    app.get('/api/functionality/:projectId/:name', wrap(project.load));
    app.get('/api/functionality/:projectId/:name', wrap(functionality.get));

    app.post('/api/functionality', wrap(functionality.save));

    app.get('/api/settings', wrap(settings.load));
    app.post('/api/settings', wrap(settings.save));

    app.get('/api/autoruns', wrap(autoruns.load));
    app.post('/api/autoruns', wrap(autoruns.launch));
    app.delete('/api/autoruns/:autorunId', wrap(autoruns.remove));

    app.get('/api/versions/:projectId', wrap(project.load));
    app.get('/api/versions/:projectId', wrap(versions.get));

    app.get('/api/releases/:projectId/:version', wrap(project.load));
    app.get('/api/releases/:projectId/:version', wrap(releases.get))

    app.get('/api/topsuites/:projectId', wrap(project.load));
    app.get('/api/topsuites/:projectId', wrap(topsuites.get));

    app.get('/api/topsuites/:projectId/status/:suiteId', wrap(project.load));
    app.get('/api/topsuites/:projectId/status/:suiteId', wrap(topsuites.status));

    app.get('/api/nightlystat/:projectId', wrap(project.load));
    app.get('/api/nightlystat/:projectId', wrap(nightlystat.get));
};

function isAuthenticated(req, res, next) {
    if (req.user) {
        return next();
    } else {
        res.status(401).end();
    }
}