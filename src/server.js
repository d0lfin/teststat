/**
 * Created by dolf
 */

import 'babel-polyfill';
import Express from 'express';
import mongoose from 'mongoose';
import fs from 'fs';
import { join } from 'path';
import { DB, SERVER } from './config/config';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import express from './config/express';
import routes from './config/routes';
import monitor from './app/services/monitor';

let app = Express(),
    models = join(__dirname, 'app/models'),
    MongoStore = connectMongo(session);


fs.readdirSync(models)
    .filter((file) => {
        return ~file.indexOf('.js');
    })
    .forEach((file) => {
        require(join(models, file))
    });

mongoose.connect(DB);

app.use(session({
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    }),
    secret: 'asdsfwt34fasdfasd34563vy4',
    name: 'sessionId',
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: false
    }
}));

express(app);
routes(app);
monitor.run();

app.listen(SERVER.PORT, SERVER.IP, function () {
    console.log("Listening on " + SERVER.IP + ", server_port " + SERVER.PORT);
});