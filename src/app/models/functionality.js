import mongoose from 'mongoose';
import { TESTPALM_URL } from '../../config/config';

let FunctionalitySchema = new mongoose.Schema({
    title: String,
    active: {
        type: Boolean,
        default: false
    },
    testpalmUrl: String,
    testpalmKey: String,
    user: {
        login: String,
        name: String
    },
    tz: String,
    automated: String,
    project: {
        id: String,
        title: String
    },
    tasks: [{
        id: String,
        status: String,
        title: String,
        user: {
            login: String,
            name: String
        }
    }],
    testSuites: [{
        id: String,
        title: String,
        status: String
    }]
});

FunctionalitySchema.statics = {
    load: async function (title) {
        return await this.findOne({title : title});
    },

    loadAll: async function () {
        return await this.find({});
    },

    create: function (title, project) {
        return new this({
            title: title,
            project: {
                id: project.id,
                title: project.title
            },
            testpalmKey: project.definition.functionality.id,
            testpalmUrl: `${TESTPALM_URL}${project.id}/testcases?filters=` + encodeURI(JSON.stringify({
                type: 'EQ',
                key: 'attributes.' + project.definition.functionality.id,
                value: title
            }))
        });
    }
};

export default mongoose.model('Functionality', FunctionalitySchema);