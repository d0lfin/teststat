import mongoose from 'mongoose';

let StatusSchema = new mongoose.Schema({
    functionalityId: mongoose.Schema.Types.ObjectId,
    date: {
        type: Date,
        default: Date.now
    },
    statistics: {
        automated: {
            type: Number,
            default: 0
        },
        unautomatible: {
            type: Number,
            default: 0
        },
        unautomated: {
            type: Number,
            default: 0
        },
        deprecated: {
            type: Number,
            default: 0
        }
    },
    testpalmCases: [{
        id: String,
        title: String,
        status: [String],
        automationStatus: String,
        startrackTasks: [{
            id: String,
            title: String,
            status: String,
            user: {
                login: String,
                name: String
            }
        }]
    }]
});

StatusSchema.statics = {
    loadLastDays: async function (limit = 1) {
        let results = {
            last: null
        };
        for (let day = 0; day < limit; day++) {
            let today = new Date().toDateString(),
                from = new Date(today),
                to = new Date(today);
            from.setDate(from.getDate() - day);
            to.setDate(to.getDate() - day + 1);
            results[from.getTime()] = await this.aggregate([
                {
                    $match: {
                        date: {
                            '$lte': to,
                            '$gte': from
                        }
                    }
                }, {
                    $group: {
                        _id: "$functionalityId",
                        functionalityId: {
                            $last: "$functionalityId"
                        },
                        statistics: {
                            $last: "$statistics"
                        },
                        testpalmCases: {
                            $last: "$testpalmCases"
                        }
                    }
                }, {
                    $sort: {
                        date: -1
                    }
                }
            ]).exec();
            if (!results.last && results[from.getTime()].length > 0) {
                results.last = from.getTime().toString();
            }
        }
        return results;
    }
};

export default mongoose.model('Status', StatusSchema);