/**
 * Created by dolf
 */
class Testcase {
    constructor(project, test) {
        this._keys = {
            functionality: project.definition.functionality.id,
            status: project.definition.status.id,
            story: project.definition.story.id
        };
        this.test = test;
        this.deprecated = this._hasAttributeValue(this.test, 'Deprecated');
        if (this._hasAttributeKey(this.test, this._keys.story)) {
            this.automationStatus = 'automated';
        } else if (this._hasAttributeValue(this.test, 'Unautomatible')) {
            this.automationStatus = 'unautomatible';
        } else {
            this.automationStatus = 'unautomated';
        }
    }

    getFunctionality = function () {
        return this.test.attributes[this._keys.functionality];
    }

    getStatus = function () {
        return this.test.attributes[this._keys.status] || [];
    }

    _hasAttributeValue = function (test, v) {
        for (let key in test.attributes) {
            if (test.attributes.hasOwnProperty(key)) {
                if (test.attributes[key].indexOf(v) > -1) {
                    return true;
                }
            }
        }
        return false;
    }

    _hasAttributeKey = function (test, k) {
        for (let key in test.attributes) {
            if (test.attributes.hasOwnProperty(key)) {
                if (key === k) {
                    return true;
                }
            }
        }
        return false;
    }
}

export default Testcase;