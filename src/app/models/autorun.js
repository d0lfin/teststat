import mongoose from 'mongoose';

let AutorunSchema = new mongoose.Schema({
    testSuite: {
        id: String,
        title: String
    },
    parameters: [{
        name: String,
        value: String
    }],
    timeAdded: {
        type: Date,
        default: Date.now
    },
    reports: [String],
    timeStarted: Date,
    timeFinished: Date,
    status: String,
    buildId: String
});

AutorunSchema.statics = {
    load: async function (_id) {
        return await this.find({_id: _id});
    },
    loadAll: async function () {
        return await this.find({}).sort({
            timeAdded: 1
        });
    }
};

export default mongoose.model('Autorun', AutorunSchema);