import mongoose from 'mongoose';

let SettingsSchema = new mongoose.Schema({
    teamcityBuild: String,
    testpalmProjectId: String,
    monitorCron: String,
    oauthToken: String
});

SettingsSchema.statics = {
    load: async function () {
        let settings = await this.findOne({});
        return settings || new this();
    }
};

export default mongoose.model('Settings', SettingsSchema);