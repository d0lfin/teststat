/**
 * Created by dolf
 */
import TestCase from '../models/testcase';

async function get (req, res) {
    let versions = await req.testpalm.getVersions(req.project.id);
    let i = 0;
    let top = {};
    let result = [];
    while (versions[i]) {
        let version = versions[i];
        if (version.title.indexOf('Nightly') === -1 && !version.archived) {
            for (let j = 0; j < version.suites.length; j++) {
                let suiteId = version.suites[j];
                if (!top[suiteId]) {
                    top[suiteId] = await req.testpalm.getTestsuite(req.project.id, suiteId);
                    top[suiteId].versions = [];
                }
                top[suiteId].versions.push(version.id);
            }
        }
        i++;
    }
    for (let suiteId in top) {
        if (top.hasOwnProperty(suiteId)) {
            let suite = top[suiteId];
            result.push({
                id: suite.id,
                title: suite.title,
                status: {
                    all: 0,
                    automated: 0,
                    deprecated: 0,
                    unautomatible: 0,
                    unautomated: 0,
                    normal: 0
                },
                versions: suite.versions
            });
        }
    }
    res.json(result.sort((a, b) => a.versions.length > b.versions.length ? -1 : 1));
}

async function status (req, res) {
    let testcases = await req.testpalm.getTestsuiteTestcases(req.project.id, req.params.suiteId),
        status = {
            all: testcases.length,
            automated: 0,
            deprecated: 0,
            unautomatible: 0,
            unautomated: 0,
            normal: 0
        },
        j = 0;
    while (testcases[j]) {
        let wrapper = new TestCase(req.project, testcases[j]),
            priority = testcases[j].attributes[req.project.definition.priority.id];
        priority = priority ? priority[0] : 'None';
        if (wrapper.deprecated) {
            status.deprecated++;
        } else if (priority.toLowerCase() === 'normal') {
            status.normal++;
        } else {
            status[wrapper.automationStatus]++;
        }
        j++;
    }
    res.json(status);
}

export default { get, status }