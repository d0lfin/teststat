/**
 * Created by dolf
 */
import Settings from '../models/settings';
import monitor from '../services/monitor'

async function load (req, res) {
    let settings = await Settings.load();
    if (!settings) {
        settings = new Settings();
        await settings.save();
    }
    res.json(settings);
}

async function save (req, res) {
    let settings = await Settings.load();
    settings.teamcityBuild = req.body.teamcityBuild;
    settings.testpalmProjectId = req.body.testpalmProjectId;
    settings.monitorCron = req.body.monitorCron;
    await settings.save();
    await monitor.update();
    res.json(settings);
}

export default { load, save }