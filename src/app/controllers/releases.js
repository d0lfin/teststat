/**
 * Created by dolf
 */

async function get (req, res) {
    let testruns = await req.testpalm.getTestrunsByVersion(req.params.projectId, req.params.version);
    res.json(testruns.map(function (testsuite) {
        let groups = testsuite.testGroups,
            priorities = [],
            tests = [];
        groups.forEach(function (group) {
            group.testCases.forEach(function (test) {
                let automated = test.testCase.isAutotest,
                    id = test.testCase.id || test.testCase.derivedFromId,
                    priority = test.testCase.attributes[req.project.definition.priority.id];
                priority = priority ? priority[0] : 'None';
                tests.push({
                    id: `${id}${testsuite.title.toLowerCase().indexOf('apad') > -1 ? '-apad' : '-aphone'}`,
                    automated: automated,
                    status: test.status,
                    finishedBy: test.finishedBy,
                    priority: priority
                });
                if (priorities.indexOf(priority) == -1) {
                    priorities.push(priority);
                }
            });
        });
        let suite = {
            id: testsuite.id,
            title: testsuite.title,
            external: testsuite.launcherInfo && testsuite.launcherInfo.external,
            tests: tests,
            priorities: priorities,
            startedTime: testsuite.startedTime
        };
        if (suite.external) {
            suite.link = testsuite.launcherInfo.link;
        }
        return suite;
    }));
}

export default { get }