/**
 * Created by dolf
 */
import autorunQueue from '../services/autorun-queue';

async function launch (req, res) {
    await autorunQueue.add(req.body);
    res.json({});
}

async function load (req, res) {
    res.json(autorunQueue.getAll());
}

async function remove (req, res) {
    await autorunQueue.remove({
        _id: req.params.autorunId
    });
    res.json({});
}

export default { launch, load, remove }