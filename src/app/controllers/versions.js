/**
 * Created by dolf
 */

async function get (req, res) {
    let versions = await req.testpalm.getVersions(req.project.id);
    res.json(versions);
}

export default { get }