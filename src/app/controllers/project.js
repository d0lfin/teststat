/**
 * Created by dolf
 */
import Functionality from '../models/functionality';
import Status from '../models/status';

async function list (req, res) {
    let projects = await req.testpalm.getProjects();
    res.json(projects.sort(function (a, b) {
        return a.title.localeCompare(b.title);
    }));
}

/**
 * Загружаем информацию о проекте и о значениях его полей
 * Определяем id ключа, который соответствует полю функциональности
 * Опрелеляем id ключа, который соответствует названию кейса
 * Привязываем полученные id к проекту
 */
async function load (req, res, next) {
    let id = req.params.projectId;
    req.project = await req.testpalm.getProject(id);
    req.project.definition = await req.testpalm.getDefinition(id);
    next();
}

/**
 * Запрашиваем информацию обо всех кейсах проекта и считаем общую статистику про проекту, сколько автоматизированно,
 * а сколько нет
 */
async function get (req, res) {
    let project = req.project,
        functionalities = await Functionality.loadAll(),
        statuses = await Status.loadLastDays(14),
        i = 0;
    project.functionalities = [];
    project.statistics = {};
    for (let date in statuses) {
        if (statuses.hasOwnProperty(date) && date !== 'last') {
            if (statuses[date].length > 0) {
                project.statistics[date] = statuses[date].reduce((a, b) => {
                    a.automated += b.statistics.automated;
                    a.unautomatible += b.statistics.unautomatible;
                    a.unautomated += b.statistics.unautomated;
                    a.deprecated += b.statistics.deprecated;
                    return a;
                }, {
                    automated: 0,
                    unautomatible: 0,
                    unautomated: 0,
                    deprecated: 0
                });
            }
        }
    }
    while (functionalities[i]) {
        let functionality = functionalities[i++],
            status = statuses[statuses.last].find(s => s.functionalityId.toString() == functionality._id.toString());
        if (functionality.active && status) {
            let functionalityInfo = {
                    title: functionality.title,
                    statistics: status.statistics,
                    user: functionality.user,
                    tz: functionality.tz,
                    tasks: []
                },
                tasks = [];
            if (status.testpalmCases.length > 1) {
                tasks = status.testpalmCases.reduce((a, b) => {
                    return a.concat((b.startrackTasks || []));
                }, []);
            } else {
                tasks = status.testpalmCases[0].startrackTasks || [];
            }
            tasks.forEach((task) => {
                if (task.status !== 'closed' && !functionalityInfo.tasks.find(t => t.id === task.id)) {
                    functionalityInfo.tasks.push(task);
                }
            });
            project.functionalities.push(functionalityInfo);
        }
    }
    res.json(project);
}

export default { list, load, get}