/**
 * Created by dolf
 */
import TestCase from '../models/testcase';
import Functionality from '../models/functionality';
import Status from '../models/status';

async function get (req, res) {
    let name = req.params.name,
        project = req.project,
        statuses = await Status.loadLastDays(14),
        functionality = await Functionality.load(name);
    if (!functionality) {
        functionality = Functionality.create(name, project);
    }
    let testcases = await req.testpalm.getTestcasesByFunctionality(functionality);
    functionality.tasks = await req.startrack.getIssuesForTestpalmTestcases(testcases);
    await functionality.save();
    functionality = functionality.toObject();
    functionality.testcases = _sortTestcases(project, testcases);
    functionality.statistics = {};
    for (let date in statuses) {
        if (statuses.hasOwnProperty(date)) {
            if (date === 'last') {
                functionality.statistics[statuses.last] = {};
                for (let type in functionality.testcases) {
                    if (functionality.testcases.hasOwnProperty(type)) {
                        functionality.statistics[statuses.last][type] = functionality.testcases[type].length
                    }
                }
            } else if (date !== statuses.last) {
                let status = statuses[date].find(s => s.functionalityId.toString() == functionality._id.toString());
                if (status) {
                    functionality.statistics[date] = status.statistics;
                }
            }
        }
    }
    functionality.versions = await req.testpalm.getVersions(project.id);
    res.json(functionality);
}


function _sortTestcases (project, testcases) {
    let result = {
            automated: [],
            deprecated: [],
            unautomatible: [],
            unautomated: []
        },
        i = 0;
    while (testcases[i]) {
        let testcase = new TestCase(project, testcases[i++]);
        if (testcase.deprecated) {
            result.deprecated.push(testcase);
        }
        switch (testcase.automationStatus) {
            case 'automated':
                result.automated.push(testcase);
                break;
            case 'unautomatible':
                result.unautomatible.push(testcase);
                break;
            case 'unautomated':
                result.unautomated.push(testcase);
                break;
            default:
        }
    }
    return result;
}

async function save (req, res) {
    let suites = req.body.testSuites.filter(t => t.id),
        functionality = await Functionality.load(req.body.title),
        project = functionality.project,
        i = 0;
    project.definition = await req.testpalm.getDefinition(project.id);
    functionality.tz = req.body.tz;
    functionality.testSuites = [];
    while (suites[i]) {
        let suite = await req.testpalm.getTestsuite(project.id, suites[i].id);
        if (suite.title) {
            let cases = await req.testpalm.getTestsuiteTestcases(project.id, suites[i].id);
            cases = cases.map(t => new TestCase(project, t));
            functionality.testSuites.push({
                id: suite.id,
                title: suite.title,
                status: cases.find(t => t.getStatus().length && t.getStatus().find(s => s !== 'Ready' )) ? 'inProgress' : 'ready'
            });
        }
        i++;
    }
    if (req.body.user && req.body.user.login) {
        functionality.user = await req.staff.getUser(req.body.user.login);
    }
    await functionality.save();
    res.json(functionality);
}

export default { get, save }