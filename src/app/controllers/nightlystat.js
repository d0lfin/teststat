/**
 * Created by dolf
 */
import TestCase from '../models/testcase';

async function get (req, res) {
    const DAY = 1000 * 60 * 60 * 24;
    const SUITES = [{
            id: '578fb46c8895507f4dfe2a63',
            type: 'Aphone'
        }, {
            id: '578fb4518895507f4dfe2a60',
            type: 'Apad'
    }];

    let now = new Date(),
        project = req.project;

    now.setHours(1, 0, 0, 0);
    let today00 = now.getTime();
    now.setHours(3);
    let today03 = now.getTime();
    let days = [];
    for (let i = 0; i < 10; i++) {
        days.push({
            after: today00 - i * DAY,
            before: today03 - i * DAY,
            status: {
                broken: [],
                failed: [],
                skipped: [],
                passed: [],
                deprecated: [],
                unautomatible: [],
                unautomated: [],
                bug: []
            }
        });
    }
    let testcases = await req.testpalm.getTestsuiteTestcases(project.id, SUITES[0].id),
        eventsRequests = [];
    testcases = testcases.map(testcase => new TestCase(project, testcase));
    testcases.forEach(testcase => eventsRequests.push(req.testpalm.getTestEvents(project.id, testcase.test.id)));

    let events = await Promise.all(eventsRequests);

    days.forEach(day => {
        let dailyEvents = _getEvents(events, day),
            executed = [];
        dailyEvents.forEach(event => {
            if (event.testRunTitle.indexOf(SUITES[0].type) > -1) {
                let status = event.runTestCase.status.toLowerCase();
                day.status[status].push(_getTestcase(testcases, event.testcaseId));
                executed.push(event.testcaseId);
            }
        });
        testcases.forEach(testcase => {
            let id = testcase.test.id,
                test = _getTestcase(testcases, id);
            if (executed.indexOf(id) === -1) {
                if (testcase.deprecated) {
                    day.status.deprecated.push(test);
                } else {
                    switch (testcase.automationStatus) {
                        case 'automated':
                            day.status.bug.push(test);
                            break;
                        case 'unautomatible':
                            day.status.unautomatible.push(test);
                            break;
                        case 'unautomated':
                            day.status.unautomated.push(test);
                            break;
                        default:
                    }
                }
            }
        });
    });
    res.json({
        all: testcases.length,
        days: days.sort((a, b) => a.before > b.before ? 1 : -1)
    });
}

function _getTestcase(testcases, testcaseId) {
    var testcase = testcases.find(test => test.test.id === testcaseId);
    return {
        id: testcase.test.id,
        name: testcase.test.name
    }
}

function _getEvents(eventsResults, day) {
    let dailyEvents = [];
    eventsResults.forEach(events => events.forEach(event => {
        let time = event.runTestCase.startedTime;
        if (time > day.after && time < day.before) {
            dailyEvents.push(event);
        }
    }));
    return dailyEvents;
}

export default { get }