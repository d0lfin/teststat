/**
 * Created by dolf
 */
import request from 'request';
import { TEAMCITY_API_ENTRY_POINT, TEAMCITY_USER } from '../../config/config';
import { parseString } from 'xml2js';
import xmlbuilder from 'xmlbuilder';
import log4js from 'log4js';

class Teamcity {
    constructor () {
        this._logger = log4js.getLogger("teamcity");
    }

    /**
     * Запускаем билд
     * @param buildTypeId
     * @param parameters
     * @returns {*} - id билда
     */
    runBuild = async function (buildTypeId, parameters) {
        let xml = xmlbuilder.create({
                build: {
                    buildType: {
                        '@id': buildTypeId
                    },
                    comment: {
                        text: {
                            '#text': 'triggered by teststat'
                        }
                    },
                    properties: {
                        property: parameters.map((parameter) => {
                            return {
                                '@name': parameter.name,
                                '@value': parameter.value
                            }
                        })
                    }
                }
            }).end();
        xml = await this._request('buildQueue', 'post', xml);
        if (xml.build.$ && xml.build.$.id) {
            return xml.build.$.id;
        } else {
            this._logger.error('can not run build');
            return null;
        }
    }

    /**
     * Запрашиваем статус билда
     * @param buildId
     * @returns {*}
     */
    getBuildState = async function (buildId) {
        let xml = await this._request('buildQueue/id:' + buildId, 'get');
        if (xml.build.$ && xml.build.$.state) {
            return xml.build.$.state;
        } else {
            this._logger.error('can not get build state');
            return null;
        }
    }

    /**
     * Запрашиваем список артефактов в заданном каталоге
     * @param buildId
     * @param name
     * @returns {*}
     */
    getArtifactsChildrens = async function (buildId, name) {
        let xml = await this._request(`builds/id:${buildId}/artifacts/children/${name || ''}`, 'get');
        if (xml.files && xml.files.file) {
            return xml.files.file;
        } else {
            this._logger.error('can not get artifacts');
            return [];
        }
    }

    _request = async function (url, method, data) {
        let options = {
            url: TEAMCITY_API_ENTRY_POINT + url,
            auth: {
                user: TEAMCITY_USER.login,
                pass: TEAMCITY_USER.password
            }
        };
        if (data) {
            options.body = data;
            options.headers = {
                'Content-Type': 'application/xml'
            };
        }
        return await new Promise((resolve, reject) => {
            request[method.toLowerCase()](options, (err, response, body) => {
                if (err) {
                    this._logger.error(err);
                    reject(err);
                } else {
                    this._logger.debug(body);
                    parseString(body, (error, result) => {
                        if (error) {
                            this._logger.warn(error);
                            resolve([]);
                        } else {
                            resolve(result);
                        }
                    });
                }
            });
        });
    };
}

export default Teamcity;