/**
 * Created by dolf
 **/

import request from 'request';
import { TESTPALM_API_ENTRY_POINT } from '../../config/config';
import log4js from 'log4js';

class TestPalm {
    constructor (token) {
        this._token = token;
        this._logger = log4js.getLogger("testpalm");
    }

    /**
     * Запрос списка проектов
     * @returns {function(this:TestPalm)}
     */
    getProjects = async function () {
        return await this._get('projects/all');
    }

    /**
     * Запрос информации о проекте
     * @param projectId - id проекта
     * @returns {function(this:TestPalm)} - информация о проекте
     */
    getProject = async function (projectId) {
        return await this._get('projects/' + projectId);
    }

    /**
     * Запрос информации о проекте
     * @param projectId - id проекта
     * @returns {function(this:TestPalm)} - поля проекта со всеми возможными значения
     */
    getDefinition = async function (projectId) {
        let definition = await this._get('definition/' + projectId),
            _getKey = function (def, titles) {
                let i = 0;
                while (def[i]) {
                    let key = def[i];
                    if (titles.indexOf(key.title) > -1) {
                        return {
                            values: key.values,
                            id: key.id
                        }
                    }
                    i++;
                }
            },
            functionality = _getKey(definition, ['Functionality']);

        return {
            story: {
                id: _getKey(definition, ['story']).id
            },
            status: {
                id: _getKey(definition, ['Test status']).id
            },
            priority: {
                id: _getKey(definition, ['Priority']).id
            },
            functionality: {
                id: functionality.id,
                values: functionality.values
            }
        }
    }

    /**
     * Запрос информации о наборе тестов
     * @param projectId - id проекта
     * @param testsuiteId - id набора тестов
     * @returns {function(this:TestPalm)}
     */
    getTestsuite = async function (projectId, testsuiteId) {
        return await this._get('testsuite/' + projectId + '/' + testsuiteId);
    }

    /**
     * Запрос информации о тестах набора тесов
     * @param projectId - id проекта
     * @param testsuiteId - id набора тестов
     * @returns {function(this:TestPalm)}
     */
    getTestsuiteTestcases = async function (projectId, testsuiteId) {
        return await this._get('testcases/' + projectId + '/suite/' + testsuiteId);
    }

    getTestEvents = async function (projectId, testId) {
        return await this._get(`eventslog/${projectId}/preview?limit=60&skip=0&testcaseId=${testId}&type=PASSED` +
            `&type=SKIPPED&type=BROKEN&type=FAILED`);
    }

    /**
     * Список версий в проекте
     * @param projectId - id проекта
     * @returns {function(this:TestPalm.getVersions)}
     */
    getVersions = async function (projectId) {
        return await this._get('version/' + projectId);
    }

    /**
     * Возвращает все кейсы проекта с полями
     * @param projectId - id проекта
     * @returns {function(this:TestPalm)} - массив тесткейсов
     */
    getTestcasesPreview = async function (projectId) {
        return await this._get('testcases/' + projectId + '/preview');
    }

    /**
     * Запрос списка тестов для заданной функциональности
     * @param functionality
     */
    getTestcasesByFunctionality = async function (functionality) {
        let expression = {
                type: 'EQ',
                key: 'attributes.' + functionality.testpalmKey,
                value: functionality.title
            },
            projectId = functionality.project.id;
        return await this._get(`testcases/${projectId}?expression=${encodeURI(JSON.stringify(expression))}`);
    }

    /**
     * Запрос списка тестранов по версии
     * @param projectId
     * @param version
     * @returns {*}
     */
    getTestrunsByVersion = async function (projectId, version) {
        return await this._get(`testrun/${projectId}/preview?limit=200&skip=0&version=${encodeURI(version)}`)
    }

    _get = async function (url) {
        let options = {
            url: TESTPALM_API_ENTRY_POINT + url,
            headers: {
                'Authorization': 'OAuth ' + this._token
            }
        };
        return await new Promise((resolve, reject) => {
            request.get(options, (err, response, body) => {
                if (err) {
                    this._logger.error(err);
                    reject(err);
                } else {
                    resolve(JSON.parse(body));
                }
            });
        });
    }
}

export default TestPalm;