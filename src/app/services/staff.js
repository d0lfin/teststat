/**
 * Created by dolf
 **/

import request from 'request';
import { STAFF_API_ENTRY_POINT } from '../../config/config';
import log4js from 'log4js';

class Staff {
    constructor (token) {
        this._token = token;
        this._logger = log4js.getLogger("staff");
    }

    getUser = async function (login) {
        let data = await this._get('persons?_fields=name.first.ru,name.last.ru&login=' + login);
        if (data.result.length === 1) {
            return {
                login: login,
                name: `${data.result[0].name.last.ru} ${data.result[0].name.first.ru}`
            }
        } else {
            return {};
        }
    }

    _get = async function (url) {
        let options = {
            url: STAFF_API_ENTRY_POINT + url,
            headers: {
                'Authorization': 'OAuth ' + this._token
            }
        };
        return await new Promise((resolve, reject) => {
            request.get(options, (err, response, body) => {
                if (err) {
                    this._logger.error(err);
                    reject(err);
                } else {
                    resolve(JSON.parse(body));
                }
            });
        });
    }
}

export default Staff;