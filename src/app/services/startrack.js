/**
 * Created by dolf
 **/

import StartrackClient from 'st-client';

class Startrack {
    constructor (token) {
        this._startrack = StartrackClient.init({
            token: token
        });
    }

    getIssuesForTestpalmTestcases = async function (testcases) {
        let i = 0,
            tasks = [];
        while (testcases[i]) {
            let test = testcases[i++];
            if (test.automationTasks) {
                let j = 0;
                while (test.automationTasks[j]) {
                    let task = test.automationTasks[j++];
                    if (!tasks.some(t => t.id === task.id)) {
                        let issue = await this._startrack.getIssue(task.id),
                            user = null;
                        if (issue.assignee && issue.assignee.id) {
                            user = {
                                login: issue.assignee.id,
                                name: issue.assignee.display
                            }
                        }
                        tasks.push({
                            id: task.id,
                            status: issue.status.key,
                            title: task.title,
                            user: user
                        });
                    }
                }
            }
        }
        return tasks;
    }
}

export default Startrack;