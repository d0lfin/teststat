import Autorun from '../models/autorun';
import Teamcity from './teamcity';
import Settings from '../models/settings';

const STATUS = {
    PENDING: 'pending',
    QUEUED: 'queued',
    RUNNING: 'running',
    FINISHED: 'finished'
};

class AutorunQueue {
    constructor() {
        this._autoruns = [];
        this._teamcity = new Teamcity();
        this._updating = false;
        this._autoruns = [];
        Autorun.loadAll().then((autoruns) => {
            this._autoruns = this._autoruns.concat(autoruns);
            this._startMonitor();
        });
    }

    add = async function (autorun) {
        autorun.status = STATUS.PENDING;
        autorun.parameters.push({
            name: 'custom.build.config',
            value: 'build/configs/regression.yaml'
        });
        autorun = await new Autorun(autorun).save();
        this._autoruns.push(autorun);
    }

    remove = async function (autorun) {
        this._autoruns = this._autoruns.filter(a => a.toObject()._id != autorun._id);
        await Autorun.load(autorun._id).remove();
    }

    getAll = function () {
        return this._autoruns.sort((a, b) => {
            return a.timeAdded > b.timeAdded ? -1 : 1;
        });
    }

    _run = async function (autorun) {
        let settings = await Settings.load(),
            buildId = await this._teamcity.runBuild(settings.teamcityBuild, autorun.parameters);
        if (buildId) {
            autorun.buildId = buildId;
            autorun.timeStarted = Date.now();
            autorun.status = STATUS.QUEUED;
            await autorun.save();
        }
        this._updating = false;
    }

    isRunning = function () {
        return !!this._getActiveAutorun();
    }

    _startMonitor = function () {
        setInterval(() => {
            this._monitor();
        }, 5000);
    }

    _monitor = function () {
        if (!this._updating) {
            this._updating = true;
            if (this.isRunning()) {
                let autorun = this._getActiveAutorun();
                this._updateStatus(autorun);
            } else {
                let autoruns = this._autoruns.filter(a => a.status == STATUS.PENDING);
                if (autoruns.length) {
                    this._run(autoruns[0]);
                } else {
                    this._updating = false;
                }
            }
        }
    };

    _getActiveAutorun = function () {
        return this._autoruns.find(a => (a.status == STATUS.QUEUED || a.status == STATUS.RUNNING));
    }

    _updateStatus = async function (autorun) {
        let buildId = autorun.buildId;
        if (buildId) {
            let status = await this._teamcity.getBuildState(buildId),
                settings = await Settings.load(),
                teamcityBuild = settings.teamcityBuild;
            status = status.toLowerCase();
            if (status === STATUS.FINISHED.toLowerCase()) {
                let artifacts = await this._teamcity.getArtifactsChildrens(buildId, 'allure-report');
                autorun.reports = artifacts.map((artifact) => {
                    let name = artifact.$.name;
                    return `repository/download/${teamcityBuild}/${buildId}:id/allure-report/${name}%21/index.html#/`;
                });
                autorun.status = STATUS.FINISHED;
                autorun.timeFinished = Date.now();
                await autorun.save();
            } else if (status === STATUS.RUNNING && autorun.status === STATUS.QUEUED) {
                autorun.status = STATUS.RUNNING;
                await autorun.save();
            }
        }
        this._updating = false;
    }
}

export default new AutorunQueue;