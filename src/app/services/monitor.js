/**
 * Created by dolf
 */
import Settings from '../models/settings';
import Testpalm from './testpalm';
import Startrack from './startrack';
import Functionality from '../models/functionality';
import Status from '../models/status';
import TestCase from '../models/testcase';
import schedule from 'node-schedule';

class Monitor {
    run = async function () {
        await this._updateSettings();
        this._testpalm = new Testpalm(this._settings.oauthToken);
        this._startrack = new Startrack(this._settings.oauthToken);
        this._project = await this._testpalm.getProject(this._settings.testpalmProjectId);
        await this._schedule();
    }

    update = async function () {
        await this._updateSettings();
        this.stop();
        await this._schedule();
    }

    stop = function () {
        if (this._job) {
            this._job.cancel();
        }
    }

    _schedule = function () {
        this._job = schedule.scheduleJob(this._settings.monitorCron, () => {
            this._save();
        });
    }

    _updateSettings = async function () {
        this._settings = await Settings.load();
    }

    _save = async function () {
        this._project.definition = await this._testpalm.getDefinition(this._project.id);
        let functionalities = this._project.definition.functionality.values,
            i = 0;
        while (functionalities[i]) {
            let functionality = await Functionality.load(functionalities[i]);
            if (!functionality) {
                functionality = Functionality.create(functionalities[i], this._project);
            }
            functionality.active = false;
            let testcases = await this._testpalm.getTestcasesByFunctionality(functionality),
                status = new Status({
                    functionalityId: functionality._id,
                    testpalmCases: []
                }),
                j = 0;
            while (testcases[j]) {
                let wrapper = new TestCase(this._project, testcases[j]),
                    testcase = {
                        id: testcases[j].id,
                        title: testcases[j].name,
                        status: wrapper.getStatus(),
                        automationStatus: wrapper.automationStatus
                    };
                if (!functionality.active) {
                    if (testcase.status.indexOf('Ready') > -1 || testcase.status.indexOf('Need Info') > -1) {
                        functionality.active = true;
                    }
                }
                if (wrapper.deprecated) {
                    status.statistics.deprecated++;
                } else {
                    status.statistics[testcase.automationStatus]++;
                }
                if (['unautomated', 'automated'].some(el => el === testcase.automationStatus)) {
                    testcase.startrackTasks = await this._startrack.getIssuesForTestpalmTestcases([testcases[j]]);
                }
                status.testpalmCases.push(testcase);
                j++;
            }
            await functionality.save();
            await status.save();
            i++;
        }
    }
}

export default new Monitor;