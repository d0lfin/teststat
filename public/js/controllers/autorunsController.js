/**
 * Created by dolf
 */
angular.module('autorunsController', []).controller('AutorunsController', function($scope, API, $route) {
    var counter = 0,
        update = function () {
            API.getAutoruns().then(function (response) {
                $scope.autoruns = response.data.map((a) => {
                    if (a.timeStarted) {
                        var start = new Date(a.timeStarted).getTime();
                        if (a.timeFinished) {
                            a.time = new Date(a.timeFinished).getTime() - start;
                        } else {
                            a.time = Date.now() - start;
                        }
                    }
                    a.time = Math.round(a.time / 1000);
                    var testpalmVersion = a.parameters.find(p => p.name === 'system.bro.regression.testpalm_version');
                    if (testpalmVersion) {
                        a.testpalmVersion = testpalmVersion.value;
                    }
                    return a;
                });
            }, function (error) {
                console.log(error);
            });
        };

    update();
    var updatingInterval = setInterval(function () {
        if ($route.current.controller == 'AutorunsController') {
            var autorun = $scope.autoruns.find(a => a.status === 'started');
            if (autorun) autorun.time++;
            if (++counter % 5 === 0) {
                counter = 0;
                update();
            }
        } else {
            clearInterval(updatingInterval);
        }
    }, 1000);

    $scope.remove = function(index) {
        API.deleteAutorun($scope.autoruns[index]);
    };
});