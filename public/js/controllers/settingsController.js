/**
 * Created by dolf on 17.03.16.
 */
angular.module('settingsController', []).controller('SettingsController', function($scope, API) {
    API.getSettings().then(function (response) {
        $scope.settings = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.save = function() {
        API.saveSettings($scope.settings);
    };
});