/**
 * Created by dolf
 */
angular.module('topSuitesController', []).controller('TopSuitesController', function($scope, $rootScope, $location, API) {
    var loadStatus = function (projectId, suiteId) {
        API.getSuiteStatus(projectId, suiteId).then(function (response) {
            $scope.suites.find(function (suite) {
                return suite.id === suiteId;
            }).status = response.data;
        });
    };
    $scope.projectId = $location.search().id;

    API.getTopSuites($scope.projectId).then(function (response) {
        $scope.suites = response.data;
        $scope.suites.forEach(function (suite) {
            loadStatus($scope.projectId, suite.id);
        });
    }, function (error) {
        console.log(error);
    });
});