/**
 * Created by dolf on 17.03.16.
 */
angular.module('functionalityController', []).controller('FunctionalityController', function($scope, API, $location, $window) {
    var name = $location.search().value,
        project = $location.search().project,
        count = function (scope) {
            if (scope.functionality) {
                var state = scope.state;
                if (state.deprecated) {
                    scope.counter.automated = scope.functionality.testcases.automated.length;
                    scope.counter.unautomatible = scope.functionality.testcases.unautomatible.length;
                    scope.counter.unautomated = scope.functionality.testcases.unautomated.length;
                } else {
                    scope.counter.automated = scope.functionality.testcases.automated.filter(function (test) {
                        return !test.deprecated;
                    }).length;
                    scope.counter.unautomatible = scope.functionality.testcases.unautomatible.filter(function (test) {
                        return !test.deprecated;
                    }).length;
                    scope.counter.unautomated = scope.functionality.testcases.unautomated.filter(function (test) {
                        return !test.deprecated;
                    }).length;
                }
                scope.counter.all = 0;
                if (state.automated) {
                    scope.counter.all += scope.counter.automated;
                }
                if (state.unautomatible) {
                    scope.counter.all += scope.counter.unautomatible;
                }
                if (state.unautomated) {
                    scope.counter.all += scope.counter.unautomated;
                }
            }
        };

    $scope.state = {
        automated: true,
        deprecated: false,
        unautomatible: false,
        unautomated: true
    };

    $scope.counter = {
        automated: 0,
        unautomatible: 0,
        unautomated: 0,
        all: 0
    };
    
    $scope.$watch('state.deprecated', function () {
        count($scope);
    });
    $scope.$watch('state.automated', function () {
        count($scope);
    });
    $scope.$watch('state.unautomatible', function () {
        count($scope);
    });
    $scope.$watch('state.unautomated', function () {
        count($scope);
    });

    API.getFunctionality(project, name).then(function (response) {
        $scope.functionality = response.data;
        if (!$scope.functionality.tasks.length) {
            $scope.status = 'needTasks';
        } else if ($scope.functionality.tasks.some(t => (t.status !== 'closed') && (t.status !== 'open'))) {
            $scope.status = 'inProgress';
        } else if(!$scope.functionality.tasks.some(t => t.status !== 'closed')) {
            $scope.status = 'allClosed';
        } else if($scope.functionality.tasks.some(t => t.status !== 'open')) {
            $scope.status = 'hasOpened';
        } else {
            $scope.status = 'allOpened';
        }
        count($scope);
    }, function (error) {
        console.log(error);
    });

    $scope.save = function() {
        API.saveFunctionality($scope.functionality).then(function (response) {
            var fields = ['user', 'tz', 'testSuites'],
                i = 0;
            while (fields[i]) {
                $scope.functionality[fields[i]] = response.data[fields[i]];
                i++;
            }
            $scope.inEditMode = false;
        }, function (error) {
            if ($scope.functionality.user && !$scope.functionality.user.name) {
                delete $scope.functionality.user;
            }
            $scope.inEditMode = false;
        });
    };

    $scope.edit = function () {
        $scope.inEditMode = true;
        if ($scope.functionality.testSuites.length === 0) {
            $scope.addSuite();
        }
    };

    $scope.addSuite = function () {
        $scope.functionality.testSuites.push({});
    };

    $scope.removeSuite = function (i) {
        $scope.functionality.testSuites = $scope.functionality.testSuites.filter(function (value, index) {
            return index != i;
        })
    };

    $scope.launch = function () {
        var parameters = [];
        $scope.autorun.parameters['system.bro.regression.app_url'] = $scope.autorun.parameters['system.bro.regression.app_url'] || 'empty';
        for (var key in $scope.autorun.parameters) {
            if ($scope.autorun.parameters.hasOwnProperty(key)) {
                parameters.push({
                    name: key,
                    value: $scope.autorun.parameters[key]
                });
            }
        }
        $scope.autorun.parameters = parameters;
        API.launchAutorun($scope.autorun);
        $('#launcher').modal('hide');
    };

    $scope.showLauncher = function (index) {
        $scope.autorun = {
            testSuite: $scope.functionality.testSuites[index],
            parameters: {
                'system.bro.regression.suite_ids': $scope.functionality.testSuites[index].id
            },
            defaultParameters: {
                'system.bro.regression.suite_ids': $scope.functionality.testSuites[index].id
            }
        };
        $('#launcher').modal('show');
    };

    $scope.showAppTab = function () {
        $scope.autorun.parameters = $scope.autorun.defaultParameters;
        $('#launcher_tabs').find('a:first').tab('show')
    };

    $scope.showBuildTab = function () {
        $scope.autorun.parameters = $scope.autorun.defaultParameters;
        $('#launcher_tabs').find('a:last').tab('show')
    };

    $scope.closeLauncher = function () {
        $('#launcher').modal('hide');
    };
});