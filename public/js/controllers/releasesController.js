/**
 * Created by dolf on 17.03.16.
 */
angular.module('releasesController', []).controller('ReleasesController', function($scope, $rootScope, $location, API) {
    $scope.projectId = $location.search().id;

    API.getVersions($scope.projectId).then(function (response) {
        $scope.versions = response.data;
        $scope.selected_suites = {
            auto: true,
            hand: false
        };
    }, function (error) {
        console.log(error);
    });

    $scope.$watch('version', function (newVar, oldVar) {
        if (newVar && newVar !== oldVar) {
            $rootScope.progressbar.start();
            $rootScope.loading = true;
            API.getRelease($scope.projectId, newVar).then(function (response) {
                var priorities = [];
                $scope.testruns = response.data.map(function (testsuite) {
                    priorities = concatDistinct(testsuite.priorities, priorities);
                    testsuite.selected = testsuite.external;
                    return testsuite;
                }).sort(function (a, b) {
                    return a.title.localeCompare(b.title);
                });
                $scope.priorities = priorities.map(function (priority) {
                    return {
                        title: priority,
                        selected: true
                    }
                });
                $scope.calculate();
            }, function (error) {
                console.log(error);
            });
        }
    });

    $scope.$watch('selected_suites.auto', function (newVar) {
        if ($scope.testruns) {
            $scope.testruns.forEach(function (testsuite) {
                if (testsuite.external) {
                    testsuite.selected = newVar;
                }
            });
            $scope.calculate();
        }
    });

    $scope.$watch('selected_suites.hand', function (newVar) {
        if ($scope.testruns) {
            $scope.testruns.forEach(function (testsuite) {
                if (!testsuite.external) {
                    testsuite.selected = newVar;
                }
            });
            $scope.calculate();
        }
    });

    $scope.calculate = function () {
        $scope.statistics = {
            unautomated: [],
            finished_by_teamcity: [],
            finished_by_user: [],
            not_started: []
        };
        $scope.testruns.forEach(function (testsuite) {
            testsuite.statistics = getStatistics(testsuite.tests);
            if (testsuite.selected) {
                addToFullStatistics(testsuite.statistics);
            }
        });
        $scope.statistics.started = $scope.statistics.finished_by_teamcity.length +
            $scope.statistics.finished_by_user.length;
        $scope.statistics.all = $scope.statistics.started + $scope.statistics.not_started.length;
    };

    function getStatistics (tests) {
        var statistics = {
            all: 0,
            started: 0,
            unautomated: [],
            broken: [],
            failed: [],
            passed: [],
            finished_by_teamcity: [],
            finished_by_user: [],
            not_started: []
        };
        tests.forEach(function (test) {
            if ($scope.priorities.find(function (p) { return p.title === test.priority && p.selected })) {
                var id = test.id;
                statistics.all++;
                switch (test.status) {
                    case 'CREATED':
                        statistics.not_started.push(id);
                        break;
                    case 'SKIPPED':
                        statistics.not_started.push(id);
                        break;
                    default:
                        switch (test.status) {
                            case 'BROKEN':
                                statistics.broken.push(id);
                                break;
                            case 'FAILED':
                                statistics.failed.push(id);
                                break;
                            case 'PASSED':
                                statistics.passed.push(id);
                                break;
                        }
                        if (test.finishedBy === 'teamcity') {
                            statistics.finished_by_teamcity.push(id);
                        } else {
                            statistics.finished_by_user.push(id);
                        }
                }
                if (!test.automated) {
                    statistics.unautomated.push(id);
                }
            }
        });
        statistics.started = statistics.finished_by_teamcity.length + statistics.finished_by_user.length;
        return statistics;
    }

    function addToFullStatistics (statistics) {
        $scope.statistics.unautomated = concatDistinct(statistics.unautomated, $scope.statistics.unautomated);
        $scope.statistics.not_started = concatDistinct(statistics.not_started, $scope.statistics.not_started);
        $scope.statistics.finished_by_teamcity = concatDistinct(statistics.finished_by_teamcity, $scope.statistics.finished_by_teamcity);
        $scope.statistics.finished_by_user = concatDistinct(statistics.finished_by_user, $scope.statistics.finished_by_user);
    }

    function concatDistinct(from, to) {
        var result = [].concat(to);
        from.forEach( function (el) {
            if (result.indexOf(el) == -1) {
                result.push(el);
            }
        });
        return result;
    }
});