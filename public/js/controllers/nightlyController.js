/**
 * Created by dolf
 */
angular.module('nightlyController', []).controller('NightlyController', function($scope, $rootScope, $location, API) {
    $scope.problems = [];
    API.getNightlyStatistics('abro').then(function (response) {
        $scope.days = response.data;
        var isBrokenToday = function (id) {
            var today = response.data.days[response.data.days.length - 1],
                finder = function (test) {
                    return test.id === id;
                };
            return !!today.status.failed.find(finder) || !!today.status.broken.find(finder);
        };
        response.data.days.forEach(function (day) {
            var addProblem = function (testcase) {
                var problem = $scope.problems.find(function (p) {
                    return p.id === testcase.id;
                });
                if (problem) {
                    problem.count++;
                } else {
                    $scope.problems.push({
                        id: testcase.id,
                        name: testcase.name,
                        today: isBrokenToday(testcase.id),
                        count: 1
                    });
                }
            };
            day.status.failed.forEach(addProblem);
            day.status.broken.forEach(addProblem);
        });
        $scope.problems = $scope.problems.sort(function (a, b) {
            return a.count < b.count ? 1 : -1;
        });
    }, function (error) {
        console.log(error);
    });
});