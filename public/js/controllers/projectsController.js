/**
 * Created by dolf on 17.03.16.
 */
angular.module('projectsController', []).controller('ProjectsController', function($scope, API) {
    API.getProjects().then(function (response) {
        $scope.projects = response.data;
    }, function (error) {
        console.log(error);
    });
});