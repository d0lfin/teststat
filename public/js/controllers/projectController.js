/**
 * Created by dolf on 17.03.16.
 */
angular.module('projectController', []).controller('ProjectController', function($scope, API, $location) {
    $scope.state = {
        automated: true,
        unautomatible: false,
        unautomated: true
    };

    $scope.counter = {
        automated: 0,
        unautomatible: 0,
        unautomated: 0,
        all: 0
    };

    var id = $location.search().id,
        getIndex = function (statistics) {
            var index;
            for (var date in statistics) {
                if (statistics.hasOwnProperty(date)) {
                    date = parseInt(date);
                    if (!index || index < date) {
                        index = date;
                    }
                }
            }
            return index;
        },
        count = function (scope) {
            if (scope.project) {
                var state = scope.state,
                    last = getIndex(scope.project.statistics);
                scope.counter.automated = scope.project.statistics[last].automated;
                scope.counter.unautomatible = scope.project.statistics[last].unautomatible;
                scope.counter.unautomated = scope.project.statistics[last].unautomated;
                scope.counter.all = 0;
                if (state.automated) {
                    scope.counter.all += scope.counter.automated;
                }
                if (state.unautomatible) {
                    scope.counter.all += scope.counter.unautomatible;
                }
                if (state.unautomated) {
                    scope.counter.all += scope.counter.unautomated;
                }
            }
        };

    API.getProject(id).then(function (response) {
        $scope.project = response.data;
        count($scope);
        $scope.sortByName();
    }, function (error) {
        console.log(error);
    });

    $scope.$watch('state.automated', function () {
        count($scope);
    });

    $scope.$watch('state.unautomatible', function () {
        count($scope);
    });

    $scope.$watch('state.unautomated', function () {
        count($scope);
    });

    $scope.sortByName = function () {
        $scope.project.functionalities = $scope.project.functionalities.sort(function (a, b) {
            return a.title.localeCompare(b.title);
        });
    };

    $scope.sortByAutomated = function () {
        $scope.project.functionalities = $scope.project.functionalities.sort(function (a, b) {
            var automatedA = a.statistics.automated / (a.statistics.automated + a.statistics.unautomated),
                automatedB = b.statistics.automated / (b.statistics.automated + b.statistics.unautomated);
            return automatedA > automatedB ? -1 : 1;
        });
    };

    $scope.sortByCount = function () {
        $scope.project.functionalities = $scope.project.functionalities.sort(function (a, b) {
            var allA = a.statistics.automated + a.statistics.unautomated,
                allB = b.statistics.automated + b.statistics.unautomated;
            return allA > allB ? -1 : 1;
        });
    };
});