/**
 * Created by dolf on 17.03.16.
 */
var app = angular.module('teststatApp', [
    'projectsController',
    'settingsController',
    'projectController',
    'autorunsController',
    'releasesController',
    'topSuitesController',
    'nightlyController',
    'functionalityController',
    'apiService',
    'userService',
    'ngRoute',
    'ngProgress',
    'progressGraphDirective'
]);

app.controller('HeaderController', function($scope, $window, $rootScope, ngProgressFactory, User) {
    $rootScope.progressbar = ngProgressFactory.createInstance();
    $scope.loggedIn = User.isAuthorized();
    $scope.logout = function() {
        User.logout();
    }
});

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '/html/empty.html',
                controller: function($location, $cookies, $rootScope, User) {
                    $rootScope.loading = false;
                    $rootScope.progressbar.complete();
                    if (User.isAuthorized()) {
                        $location.url('/projects');
                    }
                }
            }).
            when('/projects', {
                templateUrl: '/html/projects.html',
                controller: 'ProjectsController'
            }).
            when('/project', {
                templateUrl: '/html/project.html',
                controller: 'ProjectController'
            }).
            when('/functionality', {
                templateUrl: '/html/functionality.html',
                controller: 'FunctionalityController'
            }).
            when('/settings', {
                templateUrl: '/html/settings.html',
                controller: 'SettingsController'
            }).
            when('/autoruns', {
                templateUrl: '/html/autoruns.html',
                controller: 'AutorunsController'
            }).
            when('/releases', {
                templateUrl: '/html/releases.html',
                controller: 'ReleasesController'
            }).
            when('/topsuites', {
                templateUrl: '/html/topsuites.html',
                controller: 'TopSuitesController'
            }).
            when('/nightly', {
                templateUrl: '/html/nightly.html',
                controller: 'NightlyController'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);

app.run(run);

function run($rootScope, $location, User) {
    $rootScope.$on('$locationChangeStart', function () {
        $rootScope.progressbar.start();
        $rootScope.loading = true;
        var restrictedPage = $.inArray($location.path(), ['/login', '/oauth', '/logout']) === -1;
        if (restrictedPage && !User.isAuthorized()) {
            $location.url('/');
        }
    });
}