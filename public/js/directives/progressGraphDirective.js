/**
 * Created by dolf
 */
angular.module('progressGraphDirective', [])
    .directive('automationstatus', function () {
        return {
            restrict: 'EA',
            transclude: false,
            scope: {
                data: '='
            },
            link: function (scope, element, attr) {
                var DAY_MS = 1000 * 60 * 60 * 24,
                    graph = null,
                    options = {
                        legend: {
                            left: {
                                position: "bottom-left"
                            }
                        },
                        height: '350px',
                        interpolation: {
                            enabled: false
                        },
                        showCurrentTime: false
                    },
                    groups = new vis.DataSet();
                
                groups.add({
                    id: 0,
                    content: 'Automated'
                });
                groups.add({
                    id: 1,
                    content: 'Unautomatible'
                });
                groups.add({
                    id: 2,
                    content: 'Unautomated'
                });
                groups.add({
                    id: 3,
                    content: 'Deprecated'
                });

                scope.$watch('data', function () {
                    if (scope.data == null) {
                        return;
                    } else {
                        options.start = getDate(getIndex(scope.data) - DAY_MS);
                        options.end = getDate(getIndex(scope.data, true) + DAY_MS);
                    }

                    if (graph != null) {
                        graph.destroy();
                    }

                    graph = new vis.Graph2d(element[0], convertStatistic(scope.data), groups, options);
                });

                scope.$watchCollection('options', function (options) {
                    if (graph == null) {
                        return;
                    }
                    graph.setOptions(options);
                });
            }
        };
    })
    .directive('nightly', function () {
        return {
            restrict: 'EA',
            transclude: false,
            scope: {
                data: '='
            },
            link: function (scope, element, attr) {
                var DAY_MS = 1000 * 60 * 60 * 24,
                    STATUSES_GROUPS = {
                        passed: 0,
                        failed: 1,
                        broken: 2,
                        bug: 3,
                        unautomated: 4,
                        unautomatible: 5
                    },
                    graph = null,
                    options = {
                        legend: {
                            left: {
                                position: "bottom-left"
                            }
                        },
                        height: '500px',
                        interpolation: {
                            enabled: false
                        },
                        showCurrentTime: false
                    },
                    groups = new vis.DataSet();

                scope.$watch('data', function () {
                    var lastDay = scope.data.days[scope.data.days.length - 1],
                        getPercent = function (status) {
                            return Math.ceil(lastDay.status[status].length / scope.data.all * 100) + '%'
                        };
                    if (scope.data == null) {
                        return;
                    } else {
                        options.start = getDate(scope.data.days[0].before - DAY_MS);
                        options.end = getDate(lastDay.before + DAY_MS);
                    }

                    if (graph != null) {
                        graph.destroy();
                    }

                    var data = scope.data.days.map(function (day) {
                        var date = getDate(day.before),
                            items = [];
                        for (var status in day.status) {
                            if (day.status.hasOwnProperty(status) && day.status[status].length > 0) {
                                items.push({
                                    x: date,
                                    y: day.status[status].length,
                                    group: STATUSES_GROUPS[status],
                                    label: {
                                        content: day.status[status].length,
                                        xOffset: 5,
                                        yOffset: -5
                                    }
                                });
                            }
                        }
                        return items;
                    }).reduce(function (a, b) {
                        return a.concat(b);
                    }, []);

                    groups.add({
                        id: 0,
                        content: 'Passed - ' + getPercent('passed')
                    });
                    groups.add({
                        id: 1,
                        content: 'Failed - ' + getPercent('failed')
                    });
                    groups.add({
                        id: 2,
                        content: 'Broken - ' + getPercent('broken')
                    });
                    groups.add({
                        id: 3,
                        content: 'Skipped - ' + getPercent('bug')
                    });
                    groups.add({
                        id: 4,
                        content: 'Unautomated - ' + getPercent('unautomated')
                    });
                    groups.add({
                        id: 5,
                        content: 'Unautomatible - ' + getPercent('unautomatible')
                    });

                    graph = new vis.Graph2d(element[0], new vis.DataSet(data), groups, options);
                });

                scope.$watchCollection('options', function (options) {
                    if (graph == null) {
                        return;
                    }
                    graph.setOptions(options);
                });
            }
        };
    });

function getDate (timestamp) {
    var d = new Date(timestamp);
    return d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2)
}

function convertStatistic (statistics) {
    var items = [];
    for (var date in statistics) {
        if (statistics.hasOwnProperty(date)) {
            var d = getDate(parseInt(date)),
                s = statistics[date];
            items.push({
                x: d,
                y: s.automated,
                group: 0,
                label: {
                    content: s.automated,
                    xOffset: 5,
                    yOffset: -5
                }
            }, {
                x: d,
                y: s.unautomatible,
                group: 1,
                label: {
                    content: s.unautomatible,
                    xOffset: 5,
                    yOffset: -5
                }
            }, {
                x: d,
                y: s.unautomated,
                group: 2,
                label: {
                    content: s.unautomated,
                    xOffset: 5,
                    yOffset: -5
                }
            }, {
                x: d,
                y: s.deprecated,
                group: 3,
                label: {
                    content: s.deprecated,
                    xOffset: 5,
                    yOffset: -5
                }
            });
        }
    }
    return new vis.DataSet(items);
}

function getIndex (statistics, last) {
    var index;
    last = last || false;
    for (var date in statistics) {
        if (statistics.hasOwnProperty(date)) {
            date = parseInt(date);
            if (!index || (last ? index < date : index > date)) {
                index = date;
            }
        }
    }
    return index;
}