/**
 * Created by dolf
 */
angular.module('userService', ['ngCookies'])
    .factory('User', function ($cookies, $window) {
        var key = 'sessionId';
        return {
            isAuthorized: function () {
                return !!$cookies.get(key);
            },
            logout: function() {
                $cookies.remove(key);
                $window.location.href = '/logout';
            }
        }
    });