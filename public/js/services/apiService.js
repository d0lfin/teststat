/**
 * Created by dolf on 17.03.16.
 */
angular.module('apiService', ['userService'])
    .factory('API', function ($http, $rootScope, User) {
        var request = function (promise) {
                return promise.then(function (value) {
                    $rootScope.progressbar.complete();
                    $rootScope.loading = false;
                    return value;
                }, function (reason) {
                    $rootScope.progressbar.complete();
                    $rootScope.loading = false;
                    if (reason.status === 401) {
                        User.logout();
                    } else {
                        return reason;
                    }
                });
            },
            get = function (url) {
                return request($http.get(url));
            },
            post = function (url, data) {
                return request($http.post(url, data));
            },
            del = function (url) {
                return request($http.delete(url));
            };
        return {
            getSettings: function () {
                return get('/api/settings');
            },
            getProjects: function () {
                return get('/api/projects');
            },
            getProject: function (id) {
                return get('/api/project/' + id);
            },
            getFunctionality: function (project, name) {
                return get('/api/functionality/' + project + '/' + name);
            },
            saveFunctionality: function (functionality) {
                var data = {
                    title: functionality.title,
                    user: functionality.user,
                    tz: functionality.tz,
                    project: functionality.project,
                    testSuites: functionality.testSuites
                };
                return post('/api/functionality', data);
            },
            saveSettings: function (settings) {
                return post('/api/settings', settings);
            },
            getAutoruns: function () {
                return get('/api/autoruns');
            },
            getNightlyStatistics: function (project) {
                return get('/api/nightlystat/' + project);
            },
            getVersions: function (project) {
                return get('/api/versions/' + project);
            },
            getTopSuites: function (project) {
                return get('/api/topsuites/' + project);
            },
            getSuiteStatus: function (project, suite) {
                return get('/api/topsuites/' + project + '/status/' + suite);
            },
            getRelease: function (project, version) {
                return get('/api/releases/' + project + '/' + version);
            },
            launchAutorun: function (autorun) {
                return post('/api/autoruns', autorun);
            },
            deleteAutorun: function (autorun) {
                return del('/api/autoruns/' + autorun._id);
            }
        }
    });